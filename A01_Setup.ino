void setup() {
  // Для дебага будем выводить отладочные сообщения в консоль
  //TODO Убрать вывод в консоль "за дабаг" (т.е. вывод только если скимпилированно с поддержкой дебага)
  Serial.begin(9600);
  Serial.println("Start");

Ethernet.begin(mac, ip, dns_server, gateway, subnet); // Инициализируем Ethernet Shield

  webserver.setDefaultCommand(&infoRequest); // дефолтная страница вывода (информация о контроллере)
  webserver.addCommand("command", &parsedRequest); // команды
  webserver.addCommand("state", &stateRequest); // выдать состояния всех устройств
  webserver.addCommand("getdev", &get1wireDevices); // получить список устройств на 1-wire
  webserver.begin();
  
  Serial.print("server is at ");
  Serial.println(Ethernet.localIP());
  
  // Настройка портов на вывод
  for (int thisPin = startPin; thisPin <=endPin; thisPin++)  {
    digitalWrite(thisPin, HIGH);
    pinMode(thisPin, OUTPUT);
    digitalWrite(thisPin, EEPROM.read(thisPin));    
  }
  //Инициализация аналоговых портов на выход, для работы с реле
  for (int thisPin = startAnOut; thisPin <=endAnOut; thisPin++)  {
    digitalWrite(thisPin, HIGH);
    pinMode(thisPin, OUTPUT);
    digitalWrite(thisPin, EEPROM.read(thisPin));    
  }

digitalWrite(15, HIGH);//подтягивающие резисторы на входе включаем
//digitalWrite(16, HIGH);//подтягивающие резисторы на входе включаем

  // Настройки 1-wire 
  sensors.begin(); // Инициализация шины 1-wire (для датчиков температуры)
  sensors.requestTemperatures(); // Перед каждым получением температуры надо ее запросить
  
  searchDevices();

//--------------------------DHT---------------------------------------------- 
   Serial.println("DHTxx test!");
   pinMode(DHTPIN, INPUT);      // устанавливает режим работы - input
   dht.begin();
//--------------------------DHT----------------------------------------------  
}
