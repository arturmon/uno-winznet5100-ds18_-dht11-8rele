/**
* Контроллер-исполнительное устройство (к проекту http://smartliving.ru/)
* Platform: Arduino UNO R3 + EthernetShield W5100
* IDE: Arduino 1.5.2
*
* исполнительные устройства (реле) подключены к Digital 5 - 9 и 17-19 (A3-A5)
* аналоговый вход 15 А1
* вход DHT 16 A2
*
* обращение по http://xx.xx.xx.xx/ выдаст справочную информацию по этому устройству (нужно для того, чтобы когда обращаешься
* по IP к устройству понять что это за контроллер и пр.)
*
* /state - состояние всез портов
* /command - выполнение команды
*         команды можно вызывать серией в 1 запросе. Например http://xx.xx.xx.xx/command?3=CLICK&4=CLICK&5=ON&6=OFF
*         только длинна строки запроса не должна привышать maxLength
* /getdev - получить список всех устройст на 1-wire
*         формат вывода: 
*                T<номер устройства на шине>:<HEX адрес устройства>:<текущая температура в градусах цельсия>;[...]
*                (пример T0:1060CF59010800E3:24.06;T1:109ABE59010800FE:24.56;)
*
**/
#include "DHT.h"
#include <Ethernet.h>
#include <SPI.h>
#include <Arduino.h>
#include "WebServer.h" // Webduino (https://github.com/sirleech/Webduino)
#include <OneWire.h>
#include <DallasTemperature.h>
#include <EEPROM.h>

#include "system.h"
System sys;
//--------------------------DHT----------------------------------------------
#define DHTPIN 16     // what pin we're connected to DHT (A2)


// Uncomment whatever type you're using!
#define DHTTYPE DHT11   // DHT 11 
//#define DHTTYPE DHT22   // DHT 22  (AM2302)
//#define DHTTYPE DHT21   // DHT 21 (AM2301)

// Connect pin 1 (on the left) of the sensor to +5V
// Connect pin 2 of the sensor to whatever your DHTPIN is
// Connect pin 4 (on the right) of the sensor to GROUND
// Connect a 10K resistor from pin 2 (data) to pin 1 (power) of the sensor

DHT dht(DHTPIN, DHTTYPE);
//--------------------------DHT----------------------------------------------

byte mac[] = { 0xDE, 0xAD, 0xBE, 0xE4, 0xDE, 0x36 }; // MAC-адрес нашего устройства
byte ip[] = { 192, 168, 10, 120 };
byte subnet[] = { 255, 255, 255, 0 };
byte gateway[] = { 192, 168, 10, 1 };
byte dns_server[] = { 192, 168, 10, 1 };
// ip-адрес удалённого сервера
byte rserver[] = { 192, 168, 10, 250 };



EthernetClient client;

// Настройки выходов Аналговые выходы имеют номера с 14-19 (А0-А5)
int startPin=5;
int endPin=9;
// Аналоговые выходы на реле 17-19 (A3-A5)
int startAnOut=17;
int endAnOut=19;
// аналоговые входы 15 16 (A1-A2)
#define MAX_PINS 1
const byte pin_array[MAX_PINS] = {1};  // номера используемых пинов
int val_array[MAX_PINS] ={ 0 };         // предыдущие значения для каждого порта
int temp_array[MAX_PINS]={0};

int startAn=1;
int endAn=1;
char buf[80];



// Pin controller for connection data pin DS18S20
#define ONE_WIRE_BUS 14 // Digital A0 pin Arduino (куда подключен выход с шины датчиков DS18X20  -------------A0---------------
#define TEMPERATURE_PRECISION 9

#define VERSION_STRING "0.1.0.6-DHT (11/21/22)"
#define COMPILE_DATE_STRING "08-06-2013"

P(Page_info) = "<html><head><meta http-equiv='refresh' content='5' ><meta http-equiv='Content-Type' content='text/html; charset=UTF-8' /><title>smartliving.ru controller " VERSION_STRING "</title></head><body>\n";
P(location_info) = "Тестовая комната"; //тут пишем физическое расположение девайса
P(pin_info) = "<b>A0</b> - 1-wire (Датчики температуры DS18B20)<br><b>A1</b> - аналоговый вход<br><b>A2</b> - DHT 11/22";
P(version_info) = VERSION_STRING ". Дата обновления: " COMPILE_DATE_STRING;

String url = String(60);
int maxLength=50; // Максимальная длинна строки запроса

#define delayClick 1000 // задержка при обычном CLICK
#define delayLClick 3000 // задержка при длинном LCLICK
#define MAX_COMMAND_LEN             (10)
#define MAX_PARAMETER_LEN           (10)
#define COMMAND_TABLE_SIZE          (9)
#define PREFIX ""

WebServer webserver(PREFIX, 80);
OneWire oneWire(ONE_WIRE_BUS);
DallasTemperature sensors(&oneWire);

// Для поиска
DeviceAddress Termometers;
float tempC; 

#define NAMELEN 32
#define VALUELEN 32

char gCommandBuffer[MAX_COMMAND_LEN + 1];
char gParamBuffer[MAX_PARAMETER_LEN + 1];
long gParamValue;

typedef struct {
  char const    *name;
  void          (*function)(WebServer &server);
} command_t;

command_t const gCommandTable[COMMAND_TABLE_SIZE] = {
//  {"LED",     commandsLed, },
  {"HELP",     commandsHelp, }, // Выводит список комманд (вызов http://xx.xx.xx.xx/command?8=HELP )
  {"ON",     commandsOn, }, // Устанавливает "1" на заданном цифровом порту (вызов http://xx.xx.xx.xx/command?8=ON )
  {"OFF",     commandsOff, }, // Устанавливает "0" на заданном цифровом порту (вызов http://xx.xx.xx.xx/command?8=OFF )
  {"STATUS",     commandsStatus, }, // Получить состояние цифрового порта (1 или 0) (вызов http://xx.xx.xx.xx/command?8=STATUS )
                                    // если вместо номера порта передать ALL (вызов http://xx.xx.xx.xx/command?ALL=STATUS ), то получим состояние всех портов (Пример вывода P3=0;P4=0;P5=0;P6=0;P7=0;P8=1;P9=1;)
  {"CLICK",     commandsClick, }, // Кратковременная "1" на порту 1сек (время настраивается) (вызов http://xx.xx.xx.xx/command?8=CLICK ) , команды можно вызывать серией в 1 запросе. Например http://xx.xx.xx.xx/command?3=CLICK&4=CLICK&5=ON&6=OFF
  {"LCLICK",     commandsLClick, }, // Кратковременная "1" на порту 3сек (время настраивается) (вызов http://xx.xx.xx.xx/command?8=LCLICK )
  {"AN", commandsAn, },          // Получить состояние аналогово порта (вызов http://xx.xx.xx.xx/command?1=AN )
  {"DHT", commandsDht, },        // Получить состояние датчика DHT (вызов http://xx.xx.xx.xx/command?2=DHT )
  {NULL,      NULL }
};
// несколько комманд ?1=AN&2=AN
/**********************************************************************************************************************
 *
 * Function:    cliProcessCommand
 *
 * Description: Look up the command in the command table. If the
 *              command is found, call the command's function. If the
 *              command is not found, output an error message.
 *
 * Notes:       
 *
 * Returns:     None.
 *
 **********************************************************************/
void cliProcessCommand(WebServer &server)
{
  int bCommandFound = false;
  int idx;

  gParamValue = strtol(gParamBuffer, NULL, 0);  // Convert the parameter to an integer value. If the parameter is empty, gParamValue becomes 0.
  for (idx = 0; gCommandTable[idx].name != NULL; idx++) {  // Search for the command in the command table until it is found or the end of the table is reached. If the command is found, break out of the loop.
    if (strcmp(gCommandTable[idx].name, gCommandBuffer) == 0) {
      bCommandFound = true;
      break;
    }
  }

  if (bCommandFound == true) {  // Если команда найдена (в массиве команд), то выполняем ее. Если нет - игнорируем
    (*gCommandTable[idx].function)(server);
  }
  else { // Command not found
    server.print("ERROR: Command not found");
  }
}


/**********************************************************************************************************************/
/* Обработчики команд */
//--------------DHT-------------------------//

void commandsDht(WebServer &server) {
  float h = dht.readHumidity();
  float t = dht.readTemperature();

 if (gParamValue == 2){
if (isnan(t) || isnan(h)) {
    server.print("Failed to read from DHT;");
  } else {
    server.print("H="); 
    server.print(h);
    server.print(";");
    server.print("T="); 
    server.print(t);
    server.print(";");
  }
}
else ErrorMessage(server);
}
//--------------DHT-------------------------//*/
void commandsAn(WebServer &server) {
  int anValueIn;
  if (gParamValue>=startAn && gParamValue<=endAn){
     anValueIn = analogRead(gParamValue);
            server.print("A");
            server.print(gParamValue);
            server.print("=");
            server.print(anValueIn);}
          else ErrorMessage(server);    
}

void commandsOn(WebServer &server) {
  if (gParamValue>=startPin && gParamValue<=endPin) {
     digitalWrite(gParamValue,LOW);
     EEPROM.write(gParamValue,LOW);}
  else  if (gParamValue>=startAnOut && gParamValue<=endAnOut){
    digitalWrite(gParamValue,LOW);
    EEPROM.write(gParamValue,LOW);}
else ErrorMessage(server);
}

void commandsOff(WebServer &server) {
  if (gParamValue>=startPin && gParamValue<=endPin) {
     digitalWrite(gParamValue,HIGH);
     EEPROM.write(gParamValue,HIGH);}
  else  if (gParamValue>=startAnOut && gParamValue<=endAnOut) {
    digitalWrite(gParamValue,HIGH);
    EEPROM.write(gParamValue,HIGH);}
else ErrorMessage(server);
}

void commandsClick(WebServer &server) {
  if (gParamValue>=startPin && gParamValue<=endPin) {
     digitalWrite(gParamValue,LOW);     
     delay(delayClick);
     digitalWrite(gParamValue,HIGH);}
     
  else if (gParamValue>=startAnOut && gParamValue<=endAnOut) {
     digitalWrite(gParamValue,LOW);     
     delay(delayClick);
     digitalWrite(gParamValue,HIGH);}
   else ErrorMessage(server);
}

void commandsLClick(WebServer &server) {
  if (gParamValue>=startPin && gParamValue<=endPin) {
     digitalWrite(gParamValue,LOW);     
     delay(delayLClick);
     digitalWrite(gParamValue,HIGH);}
  else if (gParamValue>=startAnOut && gParamValue<=endAnOut) {
     digitalWrite(gParamValue,LOW);     
     delay(delayLClick);
     digitalWrite(gParamValue,HIGH);}
else ErrorMessage(server);
}

void commandsStatus(WebServer &server) {
   if  (strcmp(gParamBuffer,  "ALL") == 0) { // выдать состояние всех пинов
//----------------------DHT------------------------------------------
  float h = dht.readHumidity();
  float t = dht.readTemperature();
  
if (isnan(t) || isnan(h)) {
    server.print("Failed to read DHT;");
  } else {
    server.print("H="); 
    server.print(h);
    server.print(";");
    server.print("T="); 
    server.print(t);
    server.print(";");
}



       for(int i=startAn; i<=endAn; i++){
       server.print("A");  // вывод данных дискретного входа
       server.print(i);
       server.print("=");
       server.print(analogRead(i));
       server.print(";");   
       }
       
            char my_st[8];  
          for(int i=startPin;i<=endPin;i++) {
            int st=digitalRead(i);
            itoa(st,my_st,10);
            server.print("P");
            server.print(i);
            server.print("=");
            server.print(my_st);
            server.print(";");
          }
           for(int i=startAnOut;i<=endAnOut;i++) {
            int st=digitalRead(i);
            itoa(st,my_st,10);
            server.print("P");
            server.print(i);
            server.print("=");
            server.print(my_st);
            server.print(";");
          }         
     
   } else { // выдать состояние только 1 пина
          if (gParamValue>=startPin && gParamValue<=endPin) {
            server.print("P");
            server.print(gParamValue);
            server.print("=");
            server.print(digitalRead(gParamValue));}
          else if (gParamValue>=startAnOut && gParamValue<=endAnOut) {
            server.print("P");
            server.print(gParamValue);
            server.print("=");
            server.print(digitalRead(gParamValue));}
          else ErrorMessage(server);
    }
}

void commandsHelp(WebServer &server) {
  int idx;
  for (idx = 0; gCommandTable[idx].name != NULL; idx++) {
    server.print(gCommandTable[idx].name);
    server.print("<br>");
  }
}

/**********************************************************************************************************************/

void ErrorMessage(WebServer &server) {
    server.print("Ошибка: Этот порт не настроен ");
}

/**********************************************************************************************************************
* Разбор запроса
**/
void parsedRequest(WebServer &server, WebServer::ConnectionType type, char *url_tail, bool tail_complete)
{
  URLPARAM_RESULT rc;
  char name[NAMELEN];
  int  name_len;
  char value[VALUELEN];
  int value_len;

  server.httpSuccess();  // this line sends the standard "we're all OK" headers back to the browser

  /* if we're handling a GET or POST, we can output our data here.
     For a HEAD request, we just stop after outputting headers. */
  if (type == WebServer::HEAD)
    return;

  if (strlen(url_tail))
    {
    while (strlen(url_tail)) // Разбор URI на составные части (выборка параметров)
      {
      rc = server.nextURLparam(&url_tail, name, NAMELEN, value, VALUELEN);
      if (rc == URLPARAM_EOS) {
  //      server.printP(Params_end);
      }
       else // Получили параметр (name) и его значение (value)
        {
        // Выполняем команды
        strcpy (gCommandBuffer, value); // параметры (значение)
        strcpy (gParamBuffer, name); // команда
        cliProcessCommand(server);
        }
      }
    }
/*    
  if (type == WebServer::POST)
  {
    server.printP(Post_params_begin);
    while (server.readPOSTparam(name, NAMELEN, value, VALUELEN))
    {
      server.print(name);
      server.printP(Parsed_item_separator);
      server.print(value);
      server.printP(Tail_end);
    }
  }
*/

}

void stateRequest(WebServer &server, WebServer::ConnectionType type, char *url_tail, bool tail_complete)
{
  strcpy (gParamBuffer, "ALL");
  commandsStatus(server);
}

/**********************************************************************************************************************
* Генерация и вывод информации об устройстве
**/
void infoRequest(WebServer &server, WebServer::ConnectionType type, char *url_tail, bool tail_complete)
{
  server.printP(Page_info);
  server.print("<b>IP-адрес: </b>");
  server.print(Ethernet.localIP());
  server.print("<br><b>Расположение:</b>");
  server.printP(location_info);
  server.print("<hr><b>Информация: </b><br>");
  server.printP(pin_info);
  server.print("<hr><b>Cостояние: </b><br>");
  strcpy (gParamBuffer, "ALL");
  commandsStatus(server);
  server.print("<hr><a href='/getdev'>1-wire устройства</a>");
  server.print("<hr><b>Команды:</b><br>");
  commandsHelp(server);
  server.print("<hr><b>Uptime: </b>");
  server.print(sys.uptime());
//  server.print(" RAM (byte): ");	
//  server.print(sys.ramFree());
//  server.print(" free of ");
//  server.print(sys.ramSize()); 
  server.print("<hr><b>Версия: </b>");
  server.printP(version_info);
}

void get1wireDevices(WebServer &server, WebServer::ConnectionType type, char *url_tail, bool tail_complete)
{
  //TODO получить все устройства на шине и выдать на страницу
   int numberOfDevices = sensors.getDeviceCount();
   sensors.begin();
   for(int i=0;i<numberOfDevices; i++) {
      if(sensors.getAddress(Termometers, i))
      {
          server.print("T");
          server.print(i);
          server.print(":");
          for (uint8_t i = 0; i < 8; i++) {
            if (Termometers[i] < 16) server.print("0");
              server.print(Termometers[i], HEX);
          }
          float tempC = sensors.getTempC(Termometers);
          server.print(":");
          server.print(tempC);
          server.print(";");
      } else {
            // not found
            server.print("NOT FOUND");
      }
    }
}
/*********************************************************************************************************************
void dht_print()
{
//--------------------------DHT-------------------------------------
  float h = dht.readHumidity();
  float t = dht.readTemperature();

  // check if returns are valid, if they are NaN (not a number) then something went wrong!
  if (isnan(t) || isnan(h)) {
    Serial.println("Failed to read from DHT");
  } else {
    Serial.print("Humidity: "); 
    Serial.print(h);
    Serial.print(" %\t");
    Serial.print("Temperature: "); 
    Serial.print(t);
    Serial.println(" *C");
  }
  delay(2000);
//--------------------------DHT-------------------------------------
}

/**********************************************************************************************************************/

